@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

IF "%1"=="" (
    echo Note: VENV path should be specified as first argument,
    echo e.g.: ^> venv.loadscripts.cmd .env
    Exit /b
) ELSE (
    set __VENV__=%1
    IF "!__VENV__:~-1!"=="/" SET __VENV__=!__VENV__:~0,-1!
    IF "!__VENV__:~-1!"=="\" SET __VENV__=!__VENV__:~0,-1!
)

echo Install virtualenv-win scripts (activate.win.bat and
echo    deactivate.win.bat) to current directory...
echo ----------------------------------------------------

hg clone https://bitbucket.org/sakkada/virtualenv-win

cp virtualenv-win/activate.win.bat %__VENV__%/Scripts/
cp virtualenv-win/deactivate.win.bat %__VENV__%/Scripts/

rm -r virtualenv-win

echo -------------------------------------------------
echo ...done.
echo Run: "call: path/to/env/Scripts/activate.win.bat"

ENDLOCAL
