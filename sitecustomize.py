import sys, os

if os.environ.get('PYTHON_SC_SETDEFAULTENCODING', None):
    print 'sakkada: setdefaultencoding to %s' % os.environ.get('PYTHON_SC_SETDEFAULTENCODING')
    sys.setdefaultencoding(os.environ.get('PYTHON_SC_SETDEFAULTENCODING'))

if os.environ.get('PYTHON_SC_WIN_UNICODE_CONSOLE', None):
    print 'sakkada: win_unicode_console.enable'
    import win_unicode_console
    win_unicode_console.enable()

if os.environ.get('PYTHON_SC_COLORAMA', None):
    print 'sakkada: colorama.init'
    from colorama import init
    init()
