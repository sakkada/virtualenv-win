Windows Python Virtualenv helper.

Tested with Activestate Python.

1.  Python files *.py assoc+ftype helper
2.  Updated activate + deactivate

Allow direct call of .py files located in PATH dirs.
Files will execute with local virtualenv python interpreter
if env is activated.

Installation:
1.  Set system wide "PYTHONBIN" variable to current system python interpreter.
    Note: run python_binary.bat to see where it is located.
2.  Run python_ftype.bat file.
    Note: by default it configured for standart installation of Python 2.7
          and python location in "C:\python27" directory, if your python binary
          located in another directory, change value of "__PYTHONDIR__"
          variable in "python_ftype.bat" batch file.
3.  Copy activate.win.bat and deactivate.win.bat to your env/Scripts directory

Steps 1 and 2 required only at first installation.
Each new virtualenv installation required only step 3 (loading updated activate
and deactivate scripts).

Usage:
Use as usual virtualenv, just run activate.win.bat to activate and
deactivate.win.bat to deactivate.

FarManager users may install FarCall plugin
(http://plugring.farmanager.com/plugin.php?pid=823) and after just run
    call: path/to/env/Scripts/activate.win.bat
    call: deactivate.win.bat
to activate and deactivate environment respectively.

------------------------------------------------------------------------------
Windows unicode processing and colorize output.

By default:

    >>> import sys
    >>> import locale
    >>>
    >>> sys.stdin.encoding              # this managed by PYTHONIOENCODING
    'cp866'
    >>> sys.stdout.encoding             # this managed by PYTHONIOENCODING too
    'cp866'
    >>> sys.getdefaultencoding()        # this managed by sys.setdefaultencoding
    'ascii'                             #   on startup or arter reload(sys)
    >>> locale.getpreferrableencoding()
    'cp1251'
    >>> sys.getfilesystemencoding()
    'mbcs'

So:
    set PYTHONIOENCODING=utf8
    python
    >>> import sys
    >>> sys.stdin.encoding
    'utf8'                              # ok
    >>> sys.stdout.encoding
    'utf8'                              # ok
    >>> sys.getdefaultencoding()
    'ascii'
    >>> reload(sys)
    >>> sys.setdefaultencoding('utf8')
    >>> sys.getdefaultencoding()
    'utf8'                              # ok

It looks like everything is ok, but only not for interactive mode, because
windows by default set codepage to cp866. if we sets chcp 65001,
we get the strange error: IOError: [Errno 2] No such file or directory, look at
http://stackoverflow.com/questions/13452916/why-do-i-get-ioerrors-when-writing-unicode-to-the-cmd-with-codepage-65001
for example (some windows internal issue).
So for non interactive mode it is ok, even if we just set PYTHONIOENCODING=utf8
without setdefaultencoding call.

For interactive mode it is usefull to set PYTHONIOENCODING=utf8 just to not
cautch annoyng Unicode*Error exceptions everytime we want to show something,
also possible ot combine set PYTHONIOENCODING=cp1251 and chcp 1251 but only
for ansi strings, any unicode symbol will breaks programm.

That's why win-unicode-console was written, it uses windows internals, and
replaces standars stdin, stdout and stderr by custom ones (using WinAPI
functions ReadConsoleW and WriteConsoleW to interact with Windows console
through UTF-16-LE encoded bytes).

Installation:
1.  "pip install win-unicode-console"
    This will install "win-unicode-console" - Python package to enable Unicode
    input and display when running Python from Windows console.
2.  "pip install colorama"
    This will install "colorama" - package, that makes ANSI escape character
    sequences (for producing colored terminal text and cursor positioning)
    work under MS Windows.
3.  Put "sitecustomize.py" file to your Python "site-packages" directory.
4.  Set required variables, listed below.

So, sitecustomize provides following features (put it somewrehe in PATH):
------------------------------------------------------------------------------
If variable "PYTHON_SC_SETDEFAULTENCODING" is set, enable win_unicode_console,
(win_unicode_console.enable()).

If variable "PYTHON_SC_SETDEFAULTENCODING" is set, run sys.setdefaultencoding
before it will be removed from sys module with specified value (usually "utf8")
Also you should set PYTHONIOENCODING={encoding} (see above and in python docs).

If variable "PYTHON_SC_COLORAMA" is set, enable "colorama" (colorama.init()).
For "Django" do not forget to set "ANSICON" environment variable to enable
color output on windows.
