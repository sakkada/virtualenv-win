@echo off

:: get parent directory without leader slash
for %%i in ("%~dp0..") do set "VIRTUAL_ENV=%%~fi"
for %%i in ("%~dp0..") do set "VIRTUAL_ENV_DIRNAME=%%~nxi"
for %%i in ("%~dp0..\..") do set "VIRTUAL_ENV_PARENT_DIRNAME=%%~nxi"

:: set/restore default values
if not defined PROMPT (
    set PROMPT=$P$G
)
if defined _OLD_VIRTUAL_PROMPT (
    set PROMPT=%_OLD_VIRTUAL_PROMPT%
)

if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH
    set PATH=%_OLD_VIRTUAL_PATH%
:ENDIFVPATH

if not defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVPYTHONHOME
    set PYTHONHOME=%_OLD_VIRTUAL_PYTHONHOME%
:ENDIFVPYTHONHOME

if defined PYTHONBIN goto ENDIFVPYTHONBIN
    for %%i in ("python.exe") do set "PYTHONBIN=%%~$PATH:i"
:ENDIFVPYTHONBIN
if not defined _OLD_VIRTUAL_PYTHONBIN goto ENDIFVPYTHONBINOLD
    set PYTHONBIN=%_OLD_VIRTUAL_PYTHONBIN%
:ENDIFVPYTHONBINOLD

:: save orig values in _OLD_VIRTUAL_*
set _OLD_VIRTUAL_PROMPT=%PROMPT%
set _OLD_VIRTUAL_PATH=%PATH%
if defined PYTHONHOME (
    set _OLD_VIRTUAL_PYTHONHOME=%PYTHONHOME%
)
set _OLD_VIRTUAL_PYTHONBIN=%PYTHONBIN%

:: save new values
set PROMPT=(%VIRTUAL_ENV_DIRNAME%) %PROMPT%
set PATH=%VIRTUAL_ENV%\Scripts;%PATH%
set PYTHONHOME=
for %%i in ("python.exe") do set "PYTHONBIN=%%~$PATH:i"

:: farmanager special variables (note: two spaces at the end)
set "PROMPT_FARCMD=(%VIRTUAL_ENV_DIRNAME%@%VIRTUAL_ENV_PARENT_DIRNAME%)  "
