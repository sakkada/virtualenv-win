## Ipython 5.1.0

## Set the color scheme (NoColor, Neutral, Linux, or LightBG).
c.InteractiveShell.colors = 'Linux'

## Override highlighting format for specific tokens
from pygments.token import Token, Keyword, Operator, Number, Error, Name
c.TerminalInteractiveShell.highlighting_style_overrides = {
    Token:              '#ffffff',
    Token.PromptNum:    '#88ff88',
    Token.OutPrompt:    '#cc0000',
    Token.OutPromptNum: '#ff8888',
    Number:             '#88ff88',
    Keyword.Namespace:  '#00ffff',
    Operator:           '#ffff00',
    Error:              '#88ffff',
    Name.Class:         '#ffff00',
    Name.Exception:     '#ffff00',
    Name.Function:      '#ffff00',
}
