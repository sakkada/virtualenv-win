Windows IPython config file.

Tested with Activestate Python and IPython 5.1.0.

Sets color theme like in IPythons<5.x and customize pygments
highlighting style (sets custom highlighting_style_overrides values).

Installation:
1.  If you do not have ".ipython/ipython_config.py" created in your
    user directory, run "ipython profile create". This will create default
    full documented "ipython_config.py" file in "C:\Users\{username}\.ipython".
2.  Merge content of "ipython_config.py" with ".ipython/ipython_config.py".
