@echo off

set __PYTHONDIR__=C:\Python27

echo Win Python files association helper
echo -----------------------------------
echo.
echo This helper sets python (*.py) files executable with python interpreter,
echo located in %%PYTHONBIN%% (updated activate script update this variable
echo with new virtualenv value)
echo.
echo Caution: Do not forget to set PYTHONBIN system evnironment variable.
echo          Usually it looks like PYTHONBIN=C:\python27\python.exe.
echo          Run "python_binary.bat" to see where it is located.
echo.
echo Create file types (assoc command):

assoc .py=Python.File
assoc .pyc=Python.CompiledFile
assoc .pyo=Python.CompiledFile
assoc .pyw=Python.NoConFile

echo.
echo Set associations (ftype command):

ftype Python.CompiledFile="%__PYTHONDIR__%\python.exe" "%%1" %%*
ftype Python.File="%%PYTHONBIN%%" "%%1" %%*
ftype Python.NoConFile="%__PYTHONDIR__%\pythonw.exe" "%%1" %%*
